source ./xs_global.sh
MAINCP=".:$ROOT/xSocket/xSocket.jar:$ROOT/xSocket/bin"
starttime=`date +%s%N | cut -b1-13`
java -cp ${MAINCP} XSocketServer
stoptime=`date +%s%N | cut -b1-13`
echo "StaticAnalysisTime for ${ver}${seed} elapsed: " `expr $stoptime - $starttime` milliseconds
